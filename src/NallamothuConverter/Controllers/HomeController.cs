﻿using Microsoft.AspNet.Mvc;
using NallamothuConverter.Models;
using System;

namespace NallamothuConverter.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Nallamothu";
            ViewData["Result"] = "";

            Converter converter = new Converter();
            return View(converter);
        }

        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {
                ViewData["Title"] = "Converted by Nallamothu";
                ViewData["Result"] = "Temperature in C = " +
                    (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
            }
            return View("Index", converter);
        }
    }
}
